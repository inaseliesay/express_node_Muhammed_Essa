var express = require('express');
var cookParser = require('cookie-parser');
var app = express();
app.use(cookParser());

app.get('/', function(req, res) {
	res.status(200).send('Welcome');
});

// Read Cookie Browser
app.get('/setCookiee', function(req, res) {
	res.cookie('cookieName', 'cookieValue');
	res.cookie('firstname', 'Enas');
	res.cookie('lastname', 'ELlithy');
	res.status(200).send('Cookie is send to browser');
});

app.get('/getCookiee', function(req, res) {
	res.status(200).send(req.cookies);
});

var server = app.listen(3000, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log('App is running on ' + host + ' Port ' + port);
});
var express = require('express');
var app = express();

app.use(express.static('public'));

app.get('/', function(req, res) {
	console.log('This Home Page');
	res.send('Welcome');
});

app.delete('/delete', function(req, res) {
	console.log('This Delete Page');
	res.send('Welcome');
});

var server = app.listen(3000, function() {
	var host = server.address().address;
	var port = server.address().port;
});
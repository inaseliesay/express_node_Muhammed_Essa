var express = require('express')
var app = express()
app.use(express.static('public'))

app.get('/index.html', function (req, res) {
	res.sendFile(__dirname + '/' + 'index.html');
})

app.get('/express_get', function (req, res) {
	response = { // Get Data From Front By Input Name
		firstname : req.query.firstname,
		lastname : req.query.lastname
	}
	console.log(response);
	res.end(JSON.stringify(response));
})

var server = app.listen(3000, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log(host,port);
})